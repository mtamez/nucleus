'use strict';
// TEST
// siege -b -c10 -t10S -d0 http://localhost:3030/status

module.exports = (Subscription) => {

  Subscription.disableRemoteMethodByName('create');     
  Subscription.disableRemoteMethodByName('replaceOrCreate');    
  Subscription.disableRemoteMethodByName('patchOrCreate'); 
  Subscription.disableRemoteMethodByName('exists');  
  Subscription.disableRemoteMethodByName('findById');  
  Subscription.disableRemoteMethodByName('find');  
  Subscription.disableRemoteMethodByName('findOne');  
  Subscription.disableRemoteMethodByName('destroyById');  
  Subscription.disableRemoteMethodByName('replaceById');  
  Subscription.disableRemoteMethodByName('prototype.patchAttributes');  
  Subscription.disableRemoteMethodByName('createChangeStream');  
  Subscription.disableRemoteMethodByName('updateAll');  
  Subscription.disableRemoteMethodByName('replaceOrCreate');   
  Subscription.disableRemoteMethodByName('count');  
  Subscription.disableRemoteMethodByName('upsertWithWhere');  


  Subscription.addCredits = require('./remoteMethod/subscription/addCredits.js');

  function result(data = {}, message = "NO_MAMES_NO_TRONO (ノ^o^)ノ", status = "success", statusCode = 200) {
    return { 
      status      : status,
      statusCode  : statusCode,
      message     : message,
      data        : data
    };
  }

  Subscription.register = (...params) => {

    const serviceId = params[0], 
    msisdn      = params[1],
    shortcode   = params[2],
    keyword     = params[3],
    carrierId   = params[4],
    mediaId     = params[5],
    publisherId = params[6],
    networkId   = params[7],
    promoId     = params[8],
    channel     = params[9],
    externalServiceId       = params[10],
    externalSubServiceId    = params[11],
    externalSubscriptionId  = params[12],
    priceCode     = params[13],
    nextBillDate  = params[14],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    const connector = Subscription.app.dataSources.postgres.connector;
   
    //const sql = `SELECT id, product_id, location_id, available, total, nuevacolumna FROM api.prueba;`;
    //const sql = `INSERT INTO api.test ("msisdn", "serviceId") VALUES(${serviceId}, ${msisdn}) RETURNING serial AS id;`;
   
    const sql = `INSERT INTO api.test ("msisdn", "serviceId") VALUES($1, $2) RETURNING serial AS id;`
    const values = [msisdn, serviceId]

    /*connector.query(sql, values, (err, resultObjects) => {
      next(err, result(resultObjects[0]));
    });*/

   connector.query(sql, values, (err, resultObjects) => {
      if(err) {
        next(err, null);
      }else {
        next(err, result(resultObjects[0]));
      }
    })

  }
  
  Subscription.cancel = (...params) => {
    const serviceId = params[0], 
    msisdn      = params[1],
    shortcode   = params[2],
    keyword     = params[3],
    carrierId   = params[4],
    channel     = params[5],
    next = params.pop(); // obtiene el ultimo parametro que es el callback

    next(null, result(params));
  }

  Subscription.extend = (...params) => {
    const serviceId   = params[0], 
    msisdn        = params[1],
    nextBillDate  = params[2],
    credits       = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.extend = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    nextBillDate    = params[2],
    credits         = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }
  
  Subscription.changeNextBillDate = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    nextBillDate    = params[2],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.addGracePeriodInDays = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    days            = params[2],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.addGracePeriodInDate = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    date            = params[2],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerBill = (...params) => {
    const serviceId = params[0], 
    msisdn      = params[1],
    amount      = params[2],
    billingCode = params[3],
    shortCode   = params[4],
    carrierId   = params[5],
    channel     = params[6],
    mediaId     = params[7],
    publisherId = params[8],
    networkId   = params[9],
    promoId     = params[10],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerFailedBill = (...params) => {
    const serviceId = params[0], 
    msisdn      = params[1],
    amount      = params[2],
    shortCode   = params[4],
    billingCode = params[3],
    mediaId     = params[5],
    channel     = params[6],
    publisherId   = params[7],
    networkId     = params[8],
    promoId       = params[9],
    errorCode     = params[10],
    errorMessage  = params[11],
    carrierErrorCode    = params[12],
    carrierErrorMessage = params[13],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerDownload = (...params) => {
    const serviceId = params[0], 
    msisdn        = params[1],
    contentId     = params[2],
    contentName   = params[4],
    portalId      = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerDownload = (...params) => {
    const serviceId = params[0], 
    msisdn        = params[1],
    contentId     = params[2],
    contentName   = params[4],
    portalId      = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerMo = (...params) => {
    const serviceId = params[0], 
    msisdn     = params[1],
    shortCode  = params[2],
    carrierId  = params[4],
    smsc       = params[3],
    text       = params[3],
    registerDate = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.registerMt = (...params) => {
    const serviceId = params[0], 
    msisdn     = params[1],
    shortCode  = params[2],
    carrierId  = params[4],
    smsc       = params[3],
    text       = params[3],
    registerDate = params[3],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.debitCredits = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    credits         = params[2],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.addCredits = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    credits         = params[2],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }

  Subscription.status = (...params) => {
    const serviceId = params[0], 
    msisdn          = params[1],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result({
       statusId : 1,
       statusMessage : "",
       credits : 500
    }));
  }
  
  Subscription.blacklist = (...params) => {
    const msisdn  = params[0], 
    carriedId     = params[1],
    next = params.pop(); // obtiene el ultimo parametro que es el callback
    
    next(null, result(params));
  }


  /*-------------------------------------------------------*/
  // Remote Hooks
  /*-------------------------------------------------------*/
  Subscription.beforeRemote('*', (ctx, cat, next) => {
    //console.log('calling ' + JSON.stringify(ctx.methodString));
    next(); 
  });

  Subscription.afterRemote('**', (ctx, account, next) => {
      if(ctx.result) {
        if(Array.isArray(ctx.result)) {
          ctx.res.body = { 'accounts': account };
        } else {
          ctx.res.body = { 'account': account };
        }
      }
      //console.log(ctx.res.body);     
      next();
  });

  /*-------------------------------------------------------*/
  // Remote Methods
  /*-------------------------------------------------------*/
  Subscription.remoteMethod('register', {
    http: { path: '/register', verb: 'GET' },
    accepts:[
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'shortcode',   type: 'number' },
      { arg: 'keyword',     type: 'string' },
      { arg: 'carrierId',   type: 'number' },
      { arg: 'mediaId',     type: 'number' },
      { arg: 'publisherId', type: 'number' },
      { arg: 'networkId',   type: 'number' },
      { arg: 'promoId',     type: 'number' },
      { arg: 'channel',     type: 'number' },
      { arg: 'externalServiceId',     type: 'string' },
      { arg: 'externalSubServiceId',  type: 'string' },
      { arg: 'externalSubscriptionId',type: 'string' },
      { arg: 'priceCode',     type: 'string' },
      { arg: 'nextBillDate',  type: 'string' },
      /*{
        arg: 'custom',
        type: 'number',
        http: function(ctx) {
          var req = ctx;
          console.log(req);
        }
      }*/
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('cancel', {
    http: { path: '/cancel', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'shortcode',   type: 'number' },
      { arg: 'keyword',     type: 'string' },
      { arg: 'carrierId',   type: 'number' },
      { arg: 'channel',     type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('extend', {
    http: { path: '/extend', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'nextBillDate',type: 'number' },
      { arg: 'credits',     type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('changeNextbillDate', {
    http: { path: '/changeNextbillDate', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'nextBillDate',type: 'number' },
    ],
     returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('addGracePeriodInDays', {
    http: { path: '/addGracePeriodInDays', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'days',type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('addGracePeriodInDate', {
    http: { path: '/addGracePeriodInDays', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'date',type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('registerBill', {
    http: { path: '/registerBill', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'amount',      type: 'number' },
      { arg: 'billingCode', type: 'string' },
      { arg: 'shortCode',   type: 'number' },
      { arg: 'carrierId',   type: 'number' },
      { arg: 'channel',     type: 'number' },
      { arg: 'mediaId',     type: 'number' },
      { arg: 'publisherId', type: 'number' },
      { arg: 'networkId',   type: 'number' },
      { arg: 'promoId',     type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('registerFailedBill', {
    http: { path: '/registerFailedBill', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'amount',      type: 'number' },
      { arg: 'shortCode',   type: 'number' },
      { arg: 'billingCode', type: 'string' },
      { arg: 'mediaId',     type: 'number' },
      { arg: 'channel',     type: 'number' },
      { arg: 'publisherId', type: 'number' },
      { arg: 'networkId',   type: 'number' },
      { arg: 'promoId',     type: 'number' },
      { arg: 'errorCode',     type: 'number' },
      { arg: 'errorMessage',  type: 'string' },
      { arg: 'carrierErrorCode',    type: 'string' },
      { arg: 'carrierErrorMessage', type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('registerDownload', {
    http: { path: '/registerDownload', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'contentId',   type: 'number' },
      { arg: 'contentName', type: 'string' },
      { arg: 'portalId',    type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('registerMo', {
    http: { path: '/registerMo', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'shortCode',   type: 'number' },
      { arg: 'carrierId',   type: 'number' },
      { arg: 'smsc',        type: 'string' },
      { arg: 'text',        type: 'string' },
      { arg: 'registerDate',type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('registerMt', {
    http: { path: '/registerMt', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'shortCode',   type: 'number' },
      { arg: 'carrierId',   type: 'number' },
      { arg: 'smsc',        type: 'string' },
      { arg: 'text',        type: 'string' },
      { arg: 'registerDate',type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('debitCredits', {
    http: { path: '/debitCredits', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'credits',     type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('addCredits', {
    http: { path: '/addCredits', verb: 'GET' },
    accepts: [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
      { arg: 'credits',     type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('status', {
    http: { path: '/status', verb: 'GET' },
    accepts:  [
      { arg: 'serviceId',   type: 'number' },
      { arg: 'msisdn',      type: 'string' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });

  Subscription.remoteMethod('blacklist', {
    http: { path: '/blacklist', verb: 'GET' },
    accepts:  [
      { arg: 'msisdn',    type: 'string' },
      { arg: 'carriedId', type: 'number' },
    ],
    returns: { arg: 'result', type: 'object', root: true },
    description: "descripción... "
  });
};
