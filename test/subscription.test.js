var app       = require('../server/server');
var request   = require('supertest');
var assert    = require('assert');
var loopback  = require('loopback');

function json(verb, url) {
  return request(app)[verb](url)
    .set('Content-Type', 'application/json')
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/);
}

describe('GET /api/v1/subscription/register', () => {
  
  it('Prueba', (done) => {
    json('get', '/api/v1/subscription/register?serviceId=22&msisdn=22')
      .send({})
      .expect(200)
      .end(function(err, res){
        assert(typeof res.body === 'object');
        assert.equal(res.body.statusCode, 200);
        console.log(res.body);
        done();
      });
  });

});