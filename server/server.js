'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');

var app = module.exports = loopback();

app.start = function() {
  // start the web server
  return app.listen(() => {
    
    app.emit('started');
    
    var baseUrl = app.get('url').replace(/\/$/, '');
   
    console.log(`
\x1b[34m================================================================
\x1b[34m  Nucleus API    ( ͡° ͜ʖ ͡°) 
\x1b[34m================================================================`);

    console.log('\x1b[36m%s: \x1b[0m' + process.env.NODE_ENV, "NODE_ENV");
    console.log('\x1b[36m%s: \x1b[0m' + baseUrl, "Web server");

    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
        console.log('\x1b[36m%s: \x1b[0m' + baseUrl + explorerPath, "Browse your REST API");
    }
    console.log("\x1b[34m================================================================");
  });
};

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function(err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module)
    app.start();
});
