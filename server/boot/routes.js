module.exports = function(app) {
  
  var router = app.loopback.Router();
  
  router.get('/status', function(req, res) {
    res.json({});
  });
  
  app.use(router);
}