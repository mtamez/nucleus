# Nucleus


```
Select the data-source to attach todo to: db (memory): En esta entrega elegimos que nuestro datasource sea memory y se guarde en ella, eso quiere decir que al cerrar nuestra app, se perderá todo lo que guardemos. Mas adelante veremos como podemos ir cambiando de datasources.
Select model’s base class: PersistedModel: PersistedModel es el modelo base de todos los modelos que vienen con Loopback, excepto Email y además nos provee de base las operaciones CRUD y nos expone los REST endpoints.
Expose todo via the REST API? Yes: Nos da la posibilidad de usar el API Explorer.

reservations/ 
├── client                       # Client JS, HTML and CSS files
│ └── README.md                  # Empty README.md file
├── package.json                 # Npm package specification
└── server                       # Node scripts and config 
 ├── boot                        # Initialization scripts
 │ └── root.js                   # Specify the contextroot
 ├── component-config.json       # Loopback components config
 ├── config.json                 # Global settings
 ├── datasources.json            # Datasource config
 ├── middleware.development.json # Middleware config for dev
 ├── middleware.json             # Middleware config
 ├── model-config.json           # Binds models to datasources
 └── server.js                   # Main application script


Queries
Loopback endpoints can also be used to query specific data. 
Here’s a selection of what is possible out of the box:
Show all campgrounds with ‘KOA’ in there name
/api/campgrounds?filter[where][name][like]=KOA
Show all reservations after or on 2017–03–22
/api/reservations?filter[where][startDate][gte]=2017-03-22
Show only the names of the campgrounds:
/api/campgrounds?filter[fields][name]=true
Show everything but the names of the campgrounds:
/api/campgrounds?filter[fields][name]
Show campgrounds and include their reservations:
/api/campgrounds?filter[include][reservations]
Show the first 2 campgrounds: 
/api/campgrounds?filter[limit]=2
Show the next 2 campgrounds: 
/api/campgrounds?filter[skip]=2&filter[limit]=2
Order campgrounds by name 
/api/campgrounds?filter[order]=name
Descending order campgrounds by name:
/api/campgrounds?filter[order]=name%20DESC

```